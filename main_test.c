#include "vector.h"

#define SUCCESS 0
#define FAILURE -1
#define BUFF_SIZE 5

char v_buffer1[BUFF_SIZE] = {"HELLO"};
int  v_buffer2[BUFF_SIZE] = {3, 78, 87, 23, 12};
float v_buffer3[BUFF_SIZE] = {7.89, 3.2, 3.76, 9.34, 1.89};

int ret_status = 0;
int main(void)
{
	struct vector v = vector(&v);
	struct vector v1 = vector(&v1);
	struct vector v2 = vector(&v2);
   	struct element element;
	struct element element1;
	struct element element2;
	struct element element3;

	ret_status = v.prepend(&v, ELEMENT((g_datatype)v_buffer1[0], CHAR));
   	ret_status = v1.prepend(&v1, ELEMENT((g_datatype)v_buffer2[0], INT));
	ret_status = v2.prepend(&v2, ELEMENT((g_datatype)v_buffer3[0], FLOAT));
   	ret_status = v.append(&v, ELEMENT((g_datatype)v_buffer1[1], CHAR));
   	ret_status = v1.append(&v1, ELEMENT((g_datatype)v_buffer2[1], INT));
	ret_status = v2.append(&v2, ELEMENT((g_datatype)v_buffer3[1], FLOAT));
	ret_status = v.append(&v, ELEMENT((g_datatype)v_buffer1[2], CHAR));
   	ret_status = v1.append(&v1, ELEMENT((g_datatype)v_buffer2[2], INT));
	ret_status = v2.append(&v2, ELEMENT((g_datatype)v_buffer3[2], FLOAT));
	ret_status = v.insert(&v, 1, ELEMENT((g_datatype)v_buffer1[4], CHAR));
   	ret_status = v1.insert(&v1, 1, ELEMENT((g_datatype)v_buffer2[4], INT));
	ret_status = v2.insert(&v2, 1, ELEMENT((g_datatype)v_buffer3[4], FLOAT));
	ret_status = v.move(&v, 4, 3);

	if (v.size(&v) == BUFF_SIZE-1 && v1.size(&v1) == BUFF_SIZE-1 && v2.size(&v2) == BUFF_SIZE-1) {
		printf("\n:vector total size - PASSED");
	} else {
		printf("\n:vector total size - FAILED");
	}

	if (ret_status == SUCCESS)
		printf("\n:value append,prepend,insert - PASSED");
	else
		printf("\n:value append,prepend,insert - FAILED");

	for_each(element, v) {
      		printf("%c ", VALUE(element).char_g);
   	}
        for_each(element1, v1) {
      		printf("%d ", VALUE(element1).int_g);
   	}
	for_each(element2, v2) {
      		printf("%f ", VALUE(element2).float_g);
   	}


	ret_status = v.get(&v, 1, &element);
	ret_status = v1.get(&v1, 1, &element1);
	ret_status = v2.get(&v2, 1, &element2);
        if (ret_status == SUCCESS)
		printf("\n:value get - PASSED");
	else
		printf("\n:value get - FAILED");

	printf("%c %d %f", element.field.char_g, element1.field.int_g, element2.field.float_g);

	ret_status = v.first(&v, &element);
	ret_status = v1.first(&v1, &element1);
	ret_status = v2.first(&v2, &element2);
        if (ret_status == SUCCESS)
		printf("\n:First element Read - PASSED");
	else
		printf("\n:First element Read - FAILED");

	printf("%c %d %f", element.field.char_g, element1.field.int_g, element2.field.float_g);

	ret_status = v.last(&v, &element);
	ret_status = v1.last(&v1, &element1);
	ret_status = v2.last(&v2, &element2);
        if (ret_status == SUCCESS)
		printf("\n:Last element Read - PASSED");
	else
		printf("\n:Last element Read - FAILED");

	printf("%c %d %f", element.field.char_g, element1.field.int_g, element2.field.float_g);

	if (!v.is_empty(&v) && !v1.is_empty(&v1) && !v2.is_empty(&v2)) {
		printf("\n:vector check empty - PASSED");
	} else {
		printf("\n:vector check empty - FAILED");
	}

	ret_status = v.set(&v, 2, ELEMENT((g_datatype)v_buffer1[3], CHAR));
   	ret_status = v.set(&v1, 2, ELEMENT((g_datatype)v_buffer2[3], INT));
	ret_status = v.set(&v2, 2, ELEMENT((g_datatype)v_buffer3[3], FLOAT));
	if (ret_status == SUCCESS)
		printf("\n:Element set  - PASSED");
	else
		printf("\n:Element set - FAILED");
	
	for_each(element, v) {
      		printf("%c ", VALUE(element).char_g);
   	}
        for_each(element1, v1) {
      		printf("%d ", VALUE(element1).int_g);
   	}
	for_each(element2, v2) {
      		printf("%f ", VALUE(element2).float_g);
   	}


        ret_status = v.clear(&v);
	/*ret_status = v.delete(&v, 1);
	ret_status = v.delete(&v, 1);
        ret_status = v.delete(&v, 1);
	ret_status = v.delete(&v, 1);*/
	if (ret_status == SUCCESS)
		printf("\n:Element clear  - PASSED");
	else
		printf("\n:Element clear - FAILED");

        ret_status = v1.chop(&v1);
	ret_status = v1.behead(&v1);
	if (ret_status == SUCCESS)
		printf("\n:vector chop/behead  - PASSED");
	else
		printf("\n:vector chop/behead - FAILED");

	for_each(element, v) {
      		printf("%c ", VALUE(element).char_g);
   	}
        for_each(element1, v1) {
      		printf("%d ", VALUE(element1).int_g);
   	}
	for_each(element2, v2) {
      		printf("%f ", VALUE(element2).float_g);
   	}

	if (!v.size(&v)) {
		printf("\n:vector size after cleared - PASSED");
	} else {
		printf("\n:vector size after cleared - FAILED");
	}

	/* Splice */
	struct vector *vector2 = v2.splice(&v2, 3);
	struct vector v4 = *vector2;
	if (vector2 != NULL) {
		printf("\n:Values of Slice vector2:");
		for_each(element1, v4) {
      			printf("%f ", VALUE(element1).float_g);
   		}
	}

	printf("\n:Values of actual vector");
	for_each(element2, v2) {
      		printf("%f ", VALUE(element2).float_g);
   	}       

	struct vector *p = &v;
	ret_status = destruct(&p); 
	if (ret_status == SUCCESS)
		printf("\n:vector destruct  - PASSED");
	else
		printf("\n:vector destruct - FAILED");

	printf("\n");
}
