#VECTOR
TARGET_NAME = VECTOR
BUILD_DIR = build

#SOURCE FILES 

SRCS = main_test.c
SRCS_OBJ = $(BUILD_DIR)/main_test.o


VECTOR_SRC = vector.c 
STATIC_LIB = $(BUILD_DIR)/libvector.a
STATIC_LIB_OBJS = $(BUILD_DIR)/vector.o

CC = gcc
CFLAGS = -c
AR = ar
MKDIR = mkdir -p

.PHONY: all static_lib

all: $(TARGET_NAME)

$(TARGET_NAME): $(STATIC_LIB) $(SRCS_OBJ) 
	$(CC) -o $@ $(STATIC_LIB_OBJS) $(SRCS_OBJ) -L$(BUILD_DIR) -lvector  

$(SRCS_OBJ): $(SRCS)
	$(CC) $(CFLAGS) -o $@ $^ 

#Static library

static_lib : $(STATIC_LIB)

$(STATIC_LIB): $(STATIC_LIB_OBJS)
	$(AR) rcs $@ $(STATIC_LIB_OBJS) 

$(BUILD_DIR)/%.o: $(VECTOR_SRC) | $(BUILD_DIR)
	$(CC) $(CFLAGS) -o $@ $^

$(BUILD_DIR):
	$(MKDIR) $@

run:
	./$(TARGET_NAME)	

clean:	
	rm -f $(TARGET_NAME)
	rm -f $(BUILD_DIR)/*.o








	
