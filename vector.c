
#include "vector.h"

#define SUCCESS 0
#define FAILURE -1

struct vector vector(struct vector *vector)
{
	vector->head = NULL;
	vector->tail = NULL;
	vector->append = append;
	vector->prepend = prepend;
	vector->insert = insert; 
	vector->chop = chop;
	vector->behead = behead; 
	vector->delete = delete;
	vector->set = set;
	vector->get = get;
	vector->is_empty = is_empty;
	vector->first = first;
	vector->last = last;
	vector->clear = clear;
	vector->destruct = destruct;
	vector->size = size;
	vector->move = move;
	vector->splice = splice;
	return *vector;

}
/* # Internal APIs # */

int append(struct vector *vector, struct element element)
{
	struct vector_node *new_node = malloc(sizeof(struct vector_node));

	if (new_node == NULL) {
		return FAILURE;
	}

	new_node->element = element;
	new_node->next = NULL;
	new_node->prev = NULL;
	if (vector->head == NULL && vector->tail == NULL) {
		vector->head = new_node;
		vector->tail = new_node;
	} else {
		new_node->prev = vector->tail;
		vector->tail->next = new_node;
		vector->tail = new_node;
	}
	
	return SUCCESS;
}


int prepend(struct vector *vector, struct element element)
{
	struct vector_node *new_node = malloc(sizeof(struct vector_node));

	if (new_node == NULL) {
		return FAILURE;
	}

	new_node->element = element;
	new_node->prev = NULL;
	new_node->next = NULL;
	if (vector->head == NULL && vector->tail == NULL) {
		vector->tail = new_node;
		vector->head = new_node;
	} else {
		new_node->next = vector->head;
		vector->head->prev = new_node;
		vector->head = new_node;
	}
	
	return SUCCESS;
}

int insert(struct vector *vector, int pos, struct element element)
{
	struct vector_node *new_node = NULL;
	struct vector_node *curr_node = NULL;

	if (vector->head == NULL || size(vector) < pos || pos <= 0) {
		return FAILURE;
	}

	new_node = malloc(sizeof(struct vector_node));
	if (new_node == NULL) {
		return FAILURE;
	}

	for(curr_node = vector->head; curr_node != NULL && (pos - 1); curr_node = curr_node->next) {
		pos--;
	}

	new_node->element = element;
	new_node->next =  curr_node; 
	new_node->prev = curr_node->prev;
	if (curr_node->prev != NULL) 
		curr_node->prev->next = new_node;
	else 
		vector->head = new_node;
	
	curr_node->prev = new_node;	
	return SUCCESS;
}

int chop(struct vector *vector)
{
	return delete(vector, vector->size(vector));
}

int behead(struct vector *vector)
{
	return delete(vector, 1);
}

int delete(struct vector *vector, int pos)
{
	struct vector_node *curr_node = NULL;
	if (size(vector) < pos || pos <= 0) {
		return FAILURE;
	}

	if (vector->head == NULL) {
		return FAILURE;
	}

	for(curr_node = vector->head; curr_node != NULL && (pos - 1); curr_node = curr_node->next) {
		pos--;
	}

	if (curr_node->next != NULL)
		curr_node->next->prev = curr_node->prev;
	else
		vector->tail = curr_node->prev;
		
	if (curr_node->prev != NULL)
		curr_node->prev->next = curr_node->next;
	else 
		vector->head = curr_node->next;
	
	free(curr_node);
	return SUCCESS;
}

/* Removes all the elements, but vector is available for other operations */ 
int clear(struct vector *vector)
{	
     	struct vector_node *curr_node = NULL;
        if (vector->head == NULL) {
		return FAILURE;
	}

	while ((curr_node = vector->head) != NULL) {
		if (curr_node->next != NULL)
			curr_node->next->prev = curr_node->prev;
		else
			vector->tail = curr_node->next;	
		vector->head = curr_node->next;
		free(curr_node);
	}
	return SUCCESS;
}

/* Frees the complete vector useful only when dynamically allocated */
int destruct(struct vector **vector)
{
	clear(*vector);
	memset(*vector, 0, sizeof(struct vector));
	return SUCCESS;
}

int move(struct vector *vector, int old_pos, int new_pos)
{
	struct vector_node *curr_node = NULL;
	int ret_status = FAILURE;
	struct element element_st;
	struct element element_1;
	if (size(vector) < old_pos || old_pos <= 0 || size(vector) < new_pos || new_pos <= 0) {
		return FAILURE;
	}

	if (vector->head == NULL) {
		return FAILURE;
	}

	ret_status = get(vector, old_pos, &element_st);

	if (new_pos <= old_pos) {
		while (new_pos <= old_pos && ret_status == SUCCESS) { 
			ret_status = get(vector, new_pos, &element_1);
			ret_status = set(vector, new_pos, element_st);
			element_st = element_1;
			new_pos++;
		}
	} else if (new_pos >= old_pos) {
		while (new_pos >= old_pos && ret_status == SUCCESS) {
			ret_status = get(vector, new_pos, &element_1);
			ret_status = set(vector, new_pos, element_st);
			element_st = element_1;
			new_pos--;
		}
	} else {
		return FAILURE;
	}
	return ret_status;
}

int set(struct vector *vector, int pos, struct element element)
{
	struct vector_node *curr_node = NULL;
	if (size(vector) < pos || pos <= 0) {
		return FAILURE;
	}

	if (vector->head == NULL) {
		return FAILURE;
	}

	for(curr_node = vector->head; curr_node != NULL && (pos - 1); curr_node = curr_node->next) {
		pos--;
	}
	curr_node->element = element;
	return SUCCESS;
}

/* Gives the element value but doesn't alter the list */
int first(struct vector *vector, struct element *element)
{
	if (vector->head == NULL) {
		return FAILURE;
	}
	*element = vector->head->element;
	return SUCCESS;
}

int last(struct vector *vector, struct element *element)
{
	if (vector->head == NULL) {
		return FAILURE;
	}
	*element = vector->tail->element;
	return SUCCESS;
}

int get(struct vector *vector, int pos, struct element *element)
{
	struct vector_node *curr_node = NULL;
	if (size(vector) < pos || pos <= 0) {
		return FAILURE;
	}

	if (vector->head == NULL) {
		return FAILURE;
	}

	for(curr_node = vector->head; curr_node != NULL && (pos - 1); curr_node = curr_node->next) {
		pos--;
	}
	*element = curr_node->element;
	return SUCCESS;
}

int is_empty(struct vector *vector)
{
	return !size(vector);
}

int size(struct vector *vector)
{
	int length = 0;
	struct vector_node *entry = NULL;
	for(entry = vector->head; entry != NULL; entry = entry->next) {
		length++;
	}
	return length;	
}

/* Splices the vector into two and returns second vector which starts from the `pos`.*/
struct vector * splice(struct vector *vector1, int pos)
{
	struct vector_node *curr_node = NULL;

	if (size(vector1) < pos || pos <= 0) {
		return NULL;
	}

	static struct vector vector2;
	vector2 = vector(&vector2);

	for(curr_node = vector1->head; curr_node != NULL && (pos - 1); curr_node = curr_node->next) {
		pos--;
	}
	vector2.head = curr_node;
	vector2.tail = vector1->tail;
	vector1->tail = curr_node->prev;
	vector1->tail->next = NULL;
	vector2.head->prev = NULL;
	return &vector2;
}
